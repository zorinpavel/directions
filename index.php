<?php
    
    require_once __DIR__."/vendor/autoload.php";
    
    
    use Input\Input;
    use Route\Advice;
    use Route\Average;
    use Route\Worst;
    
    
    $input = new Input();
    $inputData = $input->getData('data.txt');
    
    foreach($inputData as $case) {
        $caseResults = [];
        foreach($case as $adviceArray) {
            $advice = new Advice($adviceArray);
            $advice->Go();
            
            array_push($caseResults, $advice->coords);
        }
        
        $Worst = new Worst();
        $Worst->caseResults = $caseResults;
        $worstCoords = $Worst->find($caseResults);
        
        echo round($Worst->average['x'], 2)." ".round($Worst->average['y'], 2)." ".round($Worst->biggestDistance, 2)."<br>";
    }