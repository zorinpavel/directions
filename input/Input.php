<?php
    
    namespace Input;
    
    /*
     * обработка входных данных, сложим все в массив
     */
    class Input {
        
        public $peopleCount, $routeCount, $case;
        public $inputData = array();
        
        public function getData($filePath) {
            $fileData = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            
            foreach($fileData as $row) {
                if($this->routeCount++ < $this->peopleCount) {
                    array_push($this->inputData[$this->case], explode(" ", $row));
                } else
                    $this->setNewCase($row);
            }
            
            return $this->inputData;
        }
        
        /*
         * new test case
         */
        public function setNewCase($row) {
            $this->peopleCount = (int)$row;
            if($this->peopleCount == 0)
                return;
            
            $this->inputData[++$this->case] = array();
            $this->routeCount = 0;
        }
    
    }