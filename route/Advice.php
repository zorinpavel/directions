<?php
    
    namespace Route;
    
    
    class Advice {
        
        public  $coords       = [];
        public  $instructions = [];
        private $angle        = 0;
        
        
        public function Go() {
            foreach($this->instructions as $row) {
                switch($row) {
                    case 'start':
                        $this->turn(next($this->instructions));
                        break;
                    case 'walk':
                        $this->walk(next($this->instructions));
                        break;
                    case 'turn':
                        $this->turn(next($this->instructions));
                        break;
                    default:
                        next($this->instructions);
                        break;
                }
            }
        }
        
        
        private function turn($deg) {
            $this->angle = $this->angle + (float)$deg;
        }
        
        
        private function walk($distance) {
            $a = $distance * sin(deg2rad($this->angle));
            $this->coords['y'] += $a;
            
            $b = $distance * cos(deg2rad($this->angle));
            $this->coords['x'] += $b;
        }
        
        
        public function __construct($instructions) {
            $this->coords = [
                "x" => (float)$instructions[0],
                "y" => (float)$instructions[1]
            ];
            $this->instructions = array_splice($instructions, 2);
        }
    }