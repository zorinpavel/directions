<?php
    
    namespace Route;
    
    
    class Average {
        
        public $caseResults = [];
        
        public function find() {
            $averageX = 0;
            $averageY = 0;
            
            foreach($this->caseResults as $coords) {
                $averageX += $coords['x'];
                $averageY += $coords['y'];
            }
            
            return [
                "x" => $averageX / count($this->caseResults),
                "y" => $averageY / count($this->caseResults),
            ];
        }
        
    }