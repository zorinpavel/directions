<?php
    
    namespace Route;
    
    
    class Worst {
        
        public $average         = [];
        public $caseResults     = [];
        public $biggestDistance = 0;
        
        public function find() {
            $this->getAverage();
    
            $worstCoords = [
                "x" => 0,
                "y" => 0,
            ];
    
            foreach($this->caseResults as $coords) {
                $a = $coords['y'] - $this->average['y'];
                $b = $coords['x'] - $this->average['x'];

                $c = sqrt(pow($a, 2) + pow($b, 2));
                if($c >= $this->biggestDistance) {
                    $this->biggestDistance = $c;
                    $worstCoords = $coords;
                }
            }
            
            return $worstCoords;
        }
        
        /*
         * find average coords
         */
        public function getAverage() {
            $Average = new Average();
            $Average->caseResults = $this->caseResults;
            
            $this->average = $Average->find();
        }
    
    }